//
//
//  AutoCCLI
//
//  Created by MBA on 2015-10-17.
//  Copyright (c) 2015 Francis Marcogliese. All rights reserved.
//
// Script to logon and download USR files from CCLI SongSelect Classic

#include <iostream>	// For use of de cin and cout
#include <string.h>	// For use of strings
#include <stdio.h>	// For use of strings
#include <fstream>	// For use of text files
#include <curl/curl.h>	// To use cURL
#include <sstream>	// To use sstream
#include <unistd.h>	// To use sstream

using namespace std;


size_t write_data(char *ptr, size_t size, size_t nmemb, void *userdata) // Function allows HTML to be written to string
{
    ostringstream *streamTitle = (ostringstream*)userdata;
    size_t count = size * nmemb;
    streamTitle->write(ptr, count);
    return count;
}

/* Never writes anything, just returns the size presented */
size_t write_nothing(char *ptr, size_t size, size_t nmemb, void *userdata)
{
    return size * nmemb;
}

int main (void)
{
    
    /* VARIABLES */
    CURL			*curl;
    CURLcode		res;
    ostringstream   streamTitle;
    bool			criterion = true;
    int			i = 0,
				l = 1,
                songNumber = 0;
    string		slash,
				output,
				title,
				URL,
				Post,
				sourceName,
				temp,
				line,
				letter1,
				letter2,
				Token,
				urlNoTitle,
				ccliNumber,
                ccliString;
    char		*URLc,
				*Postc,
				*urlNoTitlec;
    
    /* INSTRUCTIONS */
    
    // Program startup
    
    cout << "Welcome to AutoCCLI" << endl;

    //fprintf( stderr, "cwd: %s\n", getenv("PWD") );
    char cwd[1024];
    getcwd(cwd,1024);
    

    int counter=0;
    for(int k=0; k<1024;k++)
    {
        counter++;
        if (cwd[k+8] == '/')
        {
            break;
        }
    }


    char dir[counter];
    memcpy(dir, &cwd[7], counter);
    dir[counter] = '\0';
    string dir2(dir);

    // Logon to SongSelect
    
    // Grab initial cookies
    curl = curl_easy_init();
    if(curl)
    {
        curl_easy_setopt(curl, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:15.0) Gecko/20100101 Firefox/15.0.1");
        curl_easy_setopt(curl, CURLOPT_URL, "https://profile.ccli.com/Account/SignIn");
        curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0L);
        curl_easy_setopt(curl, CURLOPT_REFERER, "https://songselect.ccli.com/");
        curl_easy_setopt(curl, CURLOPT_COOKIEJAR, "cookie.txt");
        curl_easy_setopt(curl, CURLOPT_NOBODY, 1);
        res = curl_easy_perform(curl);
        curl_easy_cleanup(curl);
    }
    
    // Grab CSRF token from cookie
    ifstream	cookie;
    cookie.open("cookie.txt");
    
    // Gets the line with the token
    for(int i=0; i<7; i++)
    {
        getline(cookie, line, '\n');
    };
    
    
    // Find Token
    size_t tokenID;
    tokenID = line.find("Token");
    Token = line.erase(0,tokenID + 6);
    cout << Token;
    
    // Gets the line with the ARRAffinity
    string line2;
    for(int i=0; i<8; i++)
    {
        getline(cookie, line2, '\n');
    };

    // Find Arra
    size_t arraID;
    string arra;
    arraID = line2.find("Affinity");
    arra = line2.erase(0,arraID + 9);
    cout << arra;
    
    // Logon with POST data
    curl = curl_easy_init();
    if(curl)
    {
        curl_easy_setopt(curl, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:15.0) Gecko/20100101 Firefox/15.0.1");
        curl_easy_setopt(curl, CURLOPT_URL, "https://profile.ccli.com/Account/SignIn");
        curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0L);
        
        char* Tokenc = curl_easy_escape(curl, Token.c_str(), 0);
        Token.assign(Tokenc);
        // insert username and password in string no punctuation
        Post = "__RequestVerificationToken=" + Token + ";ARRAffinity=" + arra + ";UserName=;Password=;RememberMe=false";
        Postc = new char [Post.size()+1];
        strcpy (Postc, Post.c_str());
        
        void curl_free( char *Tokenc );
        
        curl_easy_setopt(curl, CURLOPT_REFERER, "https://songselect.ccli.com/");
        curl_easy_setopt(curl, CURLOPT_POST, 1);
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, &write_nothing);
        curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1);
        curl_easy_setopt(curl, CURLOPT_POSTFIELDS, Postc);
        curl_easy_setopt(curl, CURLOPT_COOKIEFILE, "cookie.txt");
        curl_easy_setopt(curl, CURLOPT_COOKIEJAR, "cookie.txt");
        res = curl_easy_perform(curl);
        curl_easy_cleanup(curl);
    }
    
    // Cet CCLI numbers
    cout << "Copy paste the CCLI numbers separated by commas" << endl;
    cin >> ccliString;
    
    cout << "How many songs are there?" << endl;
    cin >> songNumber;

    ccliNumber = "1" ;
    // Capture each song based on the CCLI number
    for(l = 1; l <= songNumber; l++)
    {
        // Variable cleanup
        ostringstream streamTitle;
        criterion = true;
        ccliNumber.clear();
        title.clear();
        output.clear();
        i = 0;
        URL.clear();

        if (l==songNumber)
           ccliNumber = ccliString.substr(0, ccliString.size());
        else
        {
            int commapos = ccliString.find(",", 0);
            ccliNumber = ccliString.substr(0, commapos);
            ccliString = ccliString.substr(ccliNumber.size()+1, ccliString.size());
            
        }

        if(ccliNumber.size() <= 1)
        {
            cout << "No CCLI number found.";
            break;
        }
        urlNoTitle = "https://songselect.ccli.com/songs/" + ccliNumber + '/';
        urlNoTitlec = new char [urlNoTitle.size()+1];
        strcpy (urlNoTitlec, urlNoTitle.c_str());
        
        // Grab title of song
        curl = curl_easy_init();
        if(curl)
        {
            curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_data);
            curl_easy_setopt(curl, CURLOPT_WRITEDATA, &streamTitle);
            curl_easy_setopt(curl, CURLOPT_URL, urlNoTitlec);
            curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0);
            res = curl_easy_perform(curl);
            
            output = streamTitle.str();
            // Get title from returned HTML
            
            size_t titlePos;
            titlePos = output.find(ccliNumber) + 1;
            title = output.erase(0,titlePos + ccliNumber.size());
            
            while(criterion)
            {
                slash = output[i];
                if(slash == "/")
                {
                    criterion = false;
                }
                else
                    i++;
            }
            
            title = output.substr(0,i);
            
            curl_easy_cleanup(curl);
        }
        
        // Clear out put for more writedata
        output.clear();
        
        // Open download page and write lyrics to file
        curl = curl_easy_init();
        if(curl)
        {
            //URL = urlNoTitle + title + "/inforedirect";
            URL = urlNoTitle + title + "/lyrics/downloadusr";
            URLc = new char [URL.size()+1];
            strcpy (URLc, URL.c_str());
            curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_data);
            curl_easy_setopt(curl, CURLOPT_WRITEDATA, &streamTitle);
            curl_easy_setopt(curl, CURLOPT_URL, URLc);
            curl_easy_setopt(curl, CURLOPT_COOKIEFILE, "cookie.txt");
            curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0L);
            curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1);
            res = curl_easy_perform(curl);
            
            // Write data to string and remove extra CR
            output = streamTitle.str(); 
            for(int k=0; k<output.size()-1;k++)
            {
                char temp1 = output[k];
                char temp2 = output[k+1];
                if( temp1 == '\r' & temp2 == '\n')
                {
                    output.erase(k,1);
                }
            }
            // Remove first 3 lines of HTML in output
            int CR = 0;
            for(int k=0; k<output.size()-1;k++)
            {
                char temp1 = output[k];
                if( temp1 == '\n' )
                {
                    CR++;
                    if(CR == 3)
                    {
                        output.erase(0,k+1);
                        break;
                    }
                }
            }
            
            // Create output path
            string path = "/Users/" + dir2 + "/Desktop/" + title + ".usr";
            // Save string to text file
            ofstream songFile;
            //songFile.open(title + ".usr");
            songFile.open(path);
            songFile << output;
            songFile.close();
            
            curl_easy_cleanup(curl);
        }
        
    }

    
    cout << "Songs succesfully downloaded" << endl;
    
    system("read -n 1 -s -p \"Press any key to exit...\"");
    
    return 0;
}
